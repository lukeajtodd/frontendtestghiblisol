import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import FilmPage from '@/components/FilmPage';

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/film/:id',
      name: 'FilmPage',
      component: FilmPage
    }
  ]
})
